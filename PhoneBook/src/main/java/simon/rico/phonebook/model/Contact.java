package simon.rico.phonebook.model;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Created by Simon on 15/05/2017.
 */
@Entity
@Table(name = "contact")
@Data
public class Contact {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    private String lastName;

    private String phone;


    @Override
    public String toString() {
        return "id=" + id + ", name=" + name + ", lastName=" + lastName;
    }
}
