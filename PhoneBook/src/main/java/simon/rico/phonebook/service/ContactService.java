package simon.rico.phonebook.service;

import simon.rico.phonebook.dto.ContactDTO;
import simon.rico.phonebook.model.Contact;

import java.util.List;

/**
 * Contact service inteface.
 * Created by Simon on 15/05/2017.
 */
public interface ContactService {

    /**
     * Add new contact to phone book.
     * @param p contact to be added
     */
    void addContact(ContactDTO p);

    /**
     * Return a list of all the contacts in the phone book
     * @return list of contacts
     */
    Iterable<Contact> listContacts();

    /**
     * Find a contact using any of the fields
     * @param param search terms.
     * @return list of contacts.
     */
    List<Contact> findByAny(String param);
}
