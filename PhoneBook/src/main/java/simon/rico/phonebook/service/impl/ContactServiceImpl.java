package simon.rico.phonebook.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import simon.rico.phonebook.dto.ContactDTO;
import simon.rico.phonebook.model.Contact;
import simon.rico.phonebook.repository.ContactRepository;
import simon.rico.phonebook.service.ContactService;

import java.util.List;

/**
 * Service containing all methods for contact operations.
 * Created by Simon on 15/05/2017.
 */
@Service
public class ContactServiceImpl implements ContactService {

    @Autowired
    ContactRepository contactRepository;

    @Override
    public void addContact(ContactDTO p) {
        Contact contact = new Contact();
        contact.setName(p.getName());
        contact.setLastName(p.getLastName());
        contact.setPhone(p.getPhone());
        contactRepository.save(contact);
    }

    @Override
    public Iterable<Contact> listContacts() {
        return contactRepository.findAll();
    }

    @Override
    public List<Contact> findByAny(String param) {
        return contactRepository.findContactsByAny(param);
    }
}
