package simon.rico.phonebook.dto;

import lombok.Data;

/**
 * Created by Simon on 15/05/2017.
 */
@Data
public class ContactDTO {
    String name;
    String lastName;
    String phone;
}
