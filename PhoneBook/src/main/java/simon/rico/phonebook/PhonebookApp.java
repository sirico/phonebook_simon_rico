package simon.rico.phonebook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by Simon on 15/05/2017.
 */
@SpringBootApplication
@EnableJpaRepositories("simon.rico.phonebook.repository")
public class PhonebookApp  {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(PhonebookApp.class, args);
    }

}

