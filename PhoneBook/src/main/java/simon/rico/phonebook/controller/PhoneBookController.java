package simon.rico.phonebook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import simon.rico.phonebook.dto.ContactDTO;
import simon.rico.phonebook.service.ContactService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Phone book main controller.
 * Created by Simon on 15/05/2017.
 */
@Controller
public class PhoneBookController {

    private static final String INDEX_VIEW = "index";

    @Autowired
    ContactService contactService;

    @RequestMapping("/")
    public String index(Map<String, Object> model) {
        model.put("contacts", contactService.listContacts());
        return INDEX_VIEW;
    }

    @RequestMapping("/add")
    public void addContact(@ModelAttribute("add-form")ContactDTO contact, HttpServletResponse response) throws IOException {
        contactService.addContact(contact);
        response.sendRedirect("/");
    }

    @RequestMapping(value = "/search")
    public String search(@RequestParam String q, Map<String, Object> model) throws IOException {
        model.put("contacts", contactService.findByAny(q));
        return INDEX_VIEW;
    }

}
