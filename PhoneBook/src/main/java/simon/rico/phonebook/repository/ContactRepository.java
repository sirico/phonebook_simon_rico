package simon.rico.phonebook.repository;


import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import simon.rico.phonebook.model.Contact;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Simon on 15/05/2017.
 */
@Repository
@Transactional
public interface ContactRepository extends CrudRepository<Contact, Integer> {

    @Query("SELECT c FROM Contact c WHERE c.name = ?1 OR c.lastName = ?1 OR c.phone = ?1")
    List<Contact> findContactsByAny(String param);


}
