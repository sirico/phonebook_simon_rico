<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
  <title>PhoneBook</title>
  <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="css/styles.css">
</head>
<body>
<div class="container">
  <div class="pure-g">
    <div class="pure-u-1">
      <div class="header">
        <img class="logo" src="img/phonebook.png"/>
        <p>v 1.0</p>
      </div>

    </div>
  </div>
  <div class="pure-g">
    <div class="pure-u-sm-1 pure-u-1-3">
      <div class="box">
        <h2><i class="fa fa-user-plus"></i>New contact</h2>
        <form class="pure-form" id="add-form" action="/add" method="POST" role="form">
          <fieldset class="pure-group">
            <input type="text" class="pure-input-1-2" placeholder="First Name" name="name" required>
            <input type="text" class="pure-input-1-2" placeholder="Last Name" name="lastName" required>
            <input type="text" class="pure-input-1-2" placeholder="Phone" name="phone" required>
          </fieldset>
          <button type="submit" class="pure-button pure-input-1-2 pure-button-primary">
            <i class="fa fa-user-plus"></i>Add</button>
        </form>
      </div>
    </div>
    <div class="pure-u-sm-1 pure-u-1-3">
      <div class="box">
        <h2><i class="fa fa-search"></i>Search contact</h2>
        <form class="pure-form"  action="/search" method="GET" role="form>
          <fieldset class="pure-group">
            <input type="text" class="pure-input-1-2" name="q">
          </fieldset>
          <button type="submit" class="pure-button pure-input-1-2 pure-button-primary">
            <i class="fa fa-search"></i>Search</button>
        </form>
      </div>
    </div>
    <div class="pure-u-sm-1 pure-u-1-3">
      <div class="box">
        <h2><i class="fa fa-users"></i> Contacts</h2>

        <table class="pure-table">
          <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone</th>
          </tr>
          </thead>


          <tbody>
          <c:forEach items="${contacts}" var="contact">
            <tr>
              <td>${contact.name}</td>
              <td>${contact.lastName}</td>
              <td>${contact.phone}</td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</body>
</html>