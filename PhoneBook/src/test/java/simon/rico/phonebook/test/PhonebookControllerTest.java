package simon.rico.phonebook.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.context.WebApplicationContext;
import simon.rico.phonebook.PhonebookApp;
import simon.rico.phonebook.controller.PhoneBookController;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Test the message controller.
 * Created by Simon on 5/15/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = PhonebookApp.class)
@WebAppConfiguration
public class PhonebookControllerTest {

  /**
   * Web application Context for Unit Test
   */
  @Autowired
  private WebApplicationContext context;

  /**
   * Mock MVC for Unit Testing
   */
  private MockMvc mvc;

  /**
   * Phone book controller with inject mocks.
   */
  @InjectMocks
  private PhoneBookController phoneBookController;



  /**
   * Set up the tests.
   */
  @Before
  public void setUp() {
    this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
  }



  /**
   * Test the Get message method.
   *
   * @throws Exception
   */
  @Test
  public void getMessage() throws Exception {
    this.mvc.perform(get("/")).andExpect(status().isOk());

    this.mvc.perform(get("/test")).andExpect(status().isNotFound());
  }







  }